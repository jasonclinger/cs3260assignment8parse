//
//  ViewController.m
//  Assignment8Parse
//
//  Created by Jason Clinger on 3/1/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "ViewController.h"
#import "AddViewController.h"
#import "customcell.h"

@interface ViewController ()

@end

@implementation ViewController

static NSString * const reuseIdentifier = @"cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.myTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    [self.view addSubview: self.myTableView];
    
    array = [NSMutableArray new];
    NSLog(@"%@", array);
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadData];
}


-(void) reloadData{
    [self get];
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return array.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    }
    
    d = array[indexPath.row];

    cell.textLabel.text = d[@"name"];
   
    
    return cell;
    
}


-(void) get{
    
    NSString *urlString = [NSString stringWithFormat: @"https://jasonparseapp.herokuapp.com/parse/classes/Game"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate: nil delegateQueue:nil];
    
    NSDictionary* pserver = @{@"name":@"Jason Clinger"};
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:pserver options:NSJSONWritingPrettyPrinted error:&error];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"jasonparseapp" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *postData = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //        self->name = [[pserver objectForKey:@"playerName"] stringValue];
        NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        array = dictionary[@"results"];
        [self.myTableView reloadData];
    }];
    [postData resume];
    

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
