//
//  customcell.h
//  Assignment8Parse
//
//  Created by Jason Clinger on 3/2/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customcell : UITableViewCell

@property (nonatomic, strong) NSString* nameCell;
@property (nonatomic, strong) NSString* colorCell;

@end
