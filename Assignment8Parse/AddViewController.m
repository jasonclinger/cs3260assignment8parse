//
//  AddViewController.m
//  Assignment8Parse
//
//  Created by Jason Clinger on 3/1/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "AddViewController.h"

@interface AddViewController ()

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    self.title2.text = [self.info objectForKey:@"title"];
    //    self.journalEntry.text = [self.info objectForKey:@"entry"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)postBtnTouched:(id)sender {
    
    [self post];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"reload" object:nil];
}


-(void) post{
    
    NSString *urlString = [NSString stringWithFormat: @"https://jasonparseapp.herokuapp.com/parse/classes/Game"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate: nil delegateQueue:nil];
    
    NSDictionary* pserver = @{@"name":self.textFieldName.text};
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:pserver options:NSJSONWritingPrettyPrinted error:&error];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"jasonparseapp" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setHTTPBody:jsonData];
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionDataTask *postData = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //self->name = [[pserver objectForKey:@"playerName"] stringValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
        
        
        
    }];
    [postData resume];
}






@end
