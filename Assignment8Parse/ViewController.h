//
//  ViewController.h
//  Assignment8Parse
//
//  Created by Jason Clinger on 3/1/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    
    NSMutableArray* array;
    NSString* name;
    NSString* color;
    id d;
}

@property (strong, nonatomic) IBOutlet UITableView *myTableView;

@end

